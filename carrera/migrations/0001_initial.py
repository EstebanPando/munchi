# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-17 20:54
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='RegistroCarrera',
            fields=[
                ('id_carrera', models.AutoField(primary_key=True, serialize=False, unique=True)),
                ('nombre', models.CharField(blank=True, max_length=100, null=True)),
                ('descripcion', models.CharField(max_length=1000)),
                ('estudiantes', models.IntegerField(validators=[django.core.validators.MinValueValidator(1)])),
                ('facultad', models.CharField(blank=True, max_length=100, null=True)),
                ('fecha', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
