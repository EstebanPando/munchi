from django.contrib import admin
from .models import *
from .forms import RegModelFormCar
# Register your models here.

class AdminRegistro (admin.ModelAdmin):
	list_display = ["nombre", "descripcion", "estudiantes", "facultad", "fecha"]
	form = RegModelFormCar
	list_filter = ["nombre"]
	list_editable = ["descripcion"]
	search_fields = ["nombre", "facultad"]

admin.site.register(RegistroCarrera, AdminRegistro)
