from django import forms
from .models import RegistroCarrera

class RegModelFormCar(forms.ModelForm):
    class Meta:
    	modelo = RegistroCarrera
    	campos =  ["nombre", "descripcion", "estudiantes", "facultad", "fecha"]

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre: # Validamos que el contenido del campo nombre no venga vacío
            if len(nombre) >= 2 and len(nombre) <= 100: # Verificamos que el contenido del campo nombre tiene un caracter 
                #raise forms.ValidationError("El campo nombre no puede ser de un caracter")
                return nombre
            else:
                raise forms.ValidationError("Numero de caracteres insuficientes")

    def clean_estudiantes(self):
        estudiantes = self.cleaned_data.get("estudiantes")
        if estudiantes:
            if estudiantes != 0 and estudiantes >= 20:
                return estudiantes
            else:
                raise forms.ValidationError("No se puede ingresar valores en 0, ni menores de 20")

    def clean_facultad(self):
        facultad = self.cleaned_data.get("facultad")
        if facultad: # Validamos que el contenido del campo nombre no venga vacío
            if len(facultad) >= 2 and len(facultad) <= 100: # Verificamos que el contenido del campo nombre tiene un caracter 
                #raise forms.ValidationError("El campo nombre no puede ser de un caracter")
                return facultad
            else:
                raise forms.ValidationError("Numero de caracteres insuficientes")