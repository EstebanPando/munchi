from django.db import models
from django.core.validators import MinValueValidator
# Create your models here.

class RegistroCarrera(models.Model):
	id_carrera = models.AutoField(primary_key=True, unique=True)
	nombre = models.CharField(max_length=100, blank=True, null=True)
	descripcion = models.CharField(max_length=1000)
	estudiantes = models.IntegerField(validators=[MinValueValidator(1)])
	facultad = models.CharField(max_length=100, blank=True, null=True)
	fecha = models.DateTimeField(auto_now_add=True, auto_now=False) # auto_now_add debe ser FALSE, porque esta fecha no es la de la creación del objeto en la base, es una fecha definida de
	# creación de la carrera, por lo tanto, puede ser anterior al la fecha de creación del objeto en la base.

	def __str__(self):
		return self.nombre

