"""munchi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from facultad import views
from asignatura import viewsAsig


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #url(r'^facultades/', views.inicio, name='facultades'),  --> Cambia los nombres de la función en las views por 'facultades'
    url(r'^facultades/', views.facultades, name='facultades'),
    url(r'^inicio/', viewsAsig.inicio, name='inicio'),  # Cambien los nombres para los HTML... si es para asignaturas, debería ser ingresoasignatura.html
]
