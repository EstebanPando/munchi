from django import forms
from .models import Registro

class RegModelForm(forms.ModelForm):
    class Meta:
    	modelo = Registro
    	campos =  ["nombre", "edad", "rut", "carrera", "fecha_ingreso"]

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre: # Validamos que el contenido del campo nombre no venga vacío
            if len(nombre) >= 2 and len(nombre) <= 100: # Verificamos que el contenido del campo nombre tiene un caracter 
                #raise forms.ValidationError("El campo nombre no puede ser de un caracter")
                return nombre
            else:
                raise forms.ValidationError("Numero de caracteres insuficientes")

    def clean_edad(self):
        edad = self.cleaned_data.get("edad")
        if edad:
            if edad != 0 and edad >= 17:
                return edad
            else:
                raise forms.ValidationError("No se puede ingresar valores en 0, o menores a 17 ")

    def clean_carrera(self):
        carrera = self.cleaned_data.get("carrera")
        if carrera: # Validamos que el contenido del campo nombre no venga vacío
            if len(carrera) >= 2 and len(carrera) <= 100: # Verificamos que el contenido del campo nombre tiene un caracter 
                #raise forms.ValidationError("El campo nombre no puede ser de un caracter")
                return carrera
            else:
                raise forms.ValidationError("Numero de caracteres insuficientes")