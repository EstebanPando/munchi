from django.db import models
from django.core.validators import MinValueValidator

# Create your models here.

class Registro(models.Model):
	id_estudiante = models.AutoField(primary_key=True, unique=True)
	nombre = models.CharField(max_length=100, blank=True, null=True)
	edad = models.PositiveIntegerField(validators=[MinValueValidator(1)])
	rut = models.IntegerField()
	carrera = models.CharField(max_length=100, blank=True, null=True)
	fecha_ingreso = models.DateTimeField(auto_now_add=True, auto_now=False)

	def __int__(self):
		return self.id_estudiante

