from django.contrib import admin
from .models import *
from .forms import RegModelForm
# Register your models here.

class AdminRegistro (admin.ModelAdmin):
	list_display = ["nombre", "edad", "rut", "carrera", "fecha_ingreso"]
	form = RegModelForm
	list_filter = ["nombre"]
	list_editable = ["carrera"]
	search_fields = ["nombre", "rut"]

admin.site.register(Registro, AdminRegistro)
