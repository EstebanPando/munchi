from django.core.validators import MinLengthValidator
from django.core.validators import MinValueValidator
from django import forms
from .models import Registradofacu
from django.db import models

## ESTAS CREANDO FORMULARIOS NO MODELOS DE DATOS, ENTONCES DEBES USAR 'forms.CharField' o ' forms.Integer0 etc... Y NO 'models.Charfield'...
## TAL COMO LO TENIAS, EL FORMULARIO NO CARGA EN LA WEB, SOLO SE VE EL BOTON DE GUARDAR

# class RegFormfacu(forms.Form): # RegForm es el nombre para el registro del formulario
#     #id_facu = models.AutoField(primary_key=True, unique=True)
#     nombrefacu = models.CharField(validators=[MinLengthValidator(2)], max_length=100)
#     descripcionfacu = models.CharField(max_length=1000)
#     ubicacionfacu = models.CharField(default='campus Chillan, Concepcion, Los Angeles, etc')
#     numeroMinDepFacu = models.PositiveIntegerField(validators=[MinValueValidator(3)])

## OTRA COSA, COMO NO VALIDAS LOS DATOS DE ENTRADA VIA FORMULARIO, AL INGRESAR DATOS MENORES A 1 CARACTER PRODUCE ERROR EL INGRESO
class RegFormfacu(forms.Form): # RegForm es el nombre para el registro del formulario
    #id_facu = models.AutoField(primary_key=True, unique=True)
    nombrefacu = forms.CharField(max_length=100)
    descripcionfacu = forms.CharField(max_length=1000)
    ubicacionfacu = forms.CharField(max_length=100)
    numeroMinDepFacu = forms.IntegerField()

class RegModelFormfacu(forms.ModelForm):
    class Meta:
        modelo = Registradofacu
        campos = ["nombrefacu", "descripcionfacu", "ubicacionfacu", "numeroMinDepFacu"]

    def clean_nombrefacu(self):
        nombrefacu = self.cleaned_data.get("nombrefacu")
        if nombrefacu:
            if len(nombrefacu) >=2 and len(nombrefacu) <= 100:
            	return
            else:
            	raise forms.ValidationError("Número de carácteres insuficientes")

    def clean_numeroMinDepFacu(self):
        numeroMinDepFacu = self.cleaned_data.get("numeroMinDepFacu")
        if numeroMinDepFacu:
            if numeroMinDepFacu > 0 and numeroMinDepFacu <= 3:
                return numeroMinDepFacu
            else:
                raise forms.ValidationError("No se puede ingresar valores en 0, ni negativos")