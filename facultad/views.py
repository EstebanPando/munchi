from django.shortcuts import render
from .forms import RegFormfacu
from .models import Registradofacu

#def inicio(request): --> Cambiemos el nombre de esta funcion por 'facultades' ya que no tiene nada que ver 'inicio'... esto afecta en las urls.py
def facultades(request):
    form = RegFormfacu(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        #id_inv2 = form_data.get("id_inv")
        nombrefacu2 = form_data.get("nombrefacu")
        descripcionfacu2 = form_data.get("descripcionfacu")
        ubicacionfacu2 = form_data.get("ubicacionfacu")
        numeroMinDepFacu2 = form_data.get("numeroMinDepFacu")
        objeto = Registradofacu.objects.create(nombrefacu=nombrefacu2, descripcionfacu=descripcionfacu2,
                                               ubicacionfacu=ubicacionfacu2, numeroMinDepFacu=numeroMinDepFacu2)

    contexto = {
    "el_formulario_facu": form,
    }
    return render(request, 'facultades.html', contexto)