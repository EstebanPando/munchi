from django.db import models
from django.core.validators import MinLengthValidator
from django.core.validators import MinValueValidator

class Registradofacu(models.Model):
    id_facu = models.AutoField(primary_key=True, unique=True)
    nombrefacu = models.CharField(validators=[MinLengthValidator(2)], max_length=100)
    descripcionfacu = models.CharField(max_length=1000)
    ubicacionfacu = models.CharField(default='campus Chillan, Concepcion, Los Angeles, etc', max_length=1000)
    numeroMinDepFacu = models.PositiveIntegerField(validators=[MinValueValidator(3)])

    objects = models.Manager # Evita la no asociación al crear objetos para la clase con el método(Modelo.objects.create)

    def __str__(self):
        return self.nombrefacu

    class Meta:
        verbose_name = 'Facultad'  # Permite cambiar los nombres presentados en el template del admin de django
        verbose_name_plural = 'Facultades'    # Permite cambiar los nombres presentados en el template del admin de django