
from django.contrib import admin
from facultad.models import *
from .forms import RegModelFormfacu

class AdminRegistradofacu(admin.ModelAdmin):
    list_display = ["id_facu", "nombrefacu", "descripcionfacu", "ubicacionfacu", "numeroMinDepFacu"]
    form = RegModelFormfacu
    list_filter = ["nombrefacu"]
    list_editable = ["descripcionfacu"]
    list_display_links = ["nombrefacu"]
    search_fields = ["nombrefacu", "id_facu"]
    #class Meta:     --> Si no lo utilizas para que lo codificas
    #   model = Registradofacu --> Si no lo utilizas para que lo codificas

admin.site.register(Registradofacu, AdminRegistradofacu)