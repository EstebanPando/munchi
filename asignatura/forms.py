from django import forms
from .models import Asignatura

class RegForm(forms.Form):
    nombre = forms.CharField(max_length=100)
    nombre_area = forms.CharField(max_length=100)
    semestre = forms.IntegerField()

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:
            if len(nombre) >=2 and len(nombre) <= 100:
                return nombre
            else:
                raise forms.ValidationError("Número de carácteres insuficientes")


    def clean_nombre_area(self):
        nombre_area = self.cleaned_data.get("nombre_area")
        if nombre_area:
            if len(nombre_area) >=2 and len(nombre_area) <= 100:
                return nombre_area
            else:
                raise forms.ValidationError("Número de carácteres insuficientes")


    def clean_semestre(self):
        semestre = self.cleaned_data.get("semestre")
        if semestre:
            if semestre > 0 and semestre <= 2:
                return semestre
            else:
                raise forms.ValidationError("No se puede ingresar valores en 0, ni negativos") # Mejorar el mensaje al usuario cuando el valor ingresado es mayor a 2


class RegModelForm(forms.ModelForm):
    class Meta:
        modelo = Asignatura
        campos = ["nombre", "nombre_area", "semestre", "fecha"]

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:
            if len(nombre) >=2 and len(nombre) <= 100:
                return nombre
        else:
            raise forms.ValidationError("Número de carácteres insuficientes")


    def clean_nombre_area(self):
        nombre_area = self.cleaned_data.get("nombre_area")
        if nombre_area:
            if len(nombre_area) >=2 and len(nombre_area) <= 100:
                return nombre_area
        else:
            raise forms.ValidationError("Número de carácteres insuficientes")


    def clean_semestre(self):
        semestre = self.cleaned_data.get("semestre")
        if semestre:
            if semestre > 0 and semestre <= 2:
                return semestre
            else:
                raise forms.ValidationError("No se puede ingresar valores en 0, ni negativos") # Mejorar el mensaje al usuario cuando el valor ingresado es mayor a 2