from django.contrib import admin
from .models import Asignatura
from .forms import *

class AdminRegistrado(admin.ModelAdmin):  #  Por qué la clase se llama 'AdminRegistrado' ?? si es para asignaturas ??
        list_display = ["nombre", "nombre_area", "semestre", "fecha"]
        form = RegModelForm
        list_filter = ["nombre_area"]
        list_editable = ["semestre"]
        search_fields = ["nombre", "nombre_area"]


# Register your models here.
admin.site.register(Asignatura, AdminRegistrado)