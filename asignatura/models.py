from django.db import models
from django.core.validators import MinValueValidator

# Create your models here.
class Asignatura(models.Model):
    id=models.AutoField(primary_key=True, unique=True)
    nombre=models.CharField(max_length=100)
    nombre_area=models.CharField(max_length=100)
    semestre=models.IntegerField(validators=[MinValueValidator(1)])
    fecha=models.DateTimeField(auto_now_add=True, auto_now=False)

    objects = models.Manager  # Evita la no asociación al crear objetos para la clase con el método(Modelo.objects.create)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Asignatura'  # Permite cambiar los nombres presentados en el template del admin de django
        verbose_name_plural = 'Asignaturas'  # Permite cambiar los nombres presentados en el
