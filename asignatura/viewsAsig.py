from django.shortcuts import render
from .forms import RegForm
from .models import Asignatura

# Create your views here.
def inicio(request):
    form = RegForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        nombre2 = form_data.get("nombre")
        nombre_area2 = form_data.get("nombre_area")
        semestre2 = form_data.get("semestre")
        objeto = Asignatura.objects.create(nombre=nombre2, nombre_area=nombre_area2,semestre=semestre2)
        
    contexto = {
    "el_formulario_asignatura": form,
    }
    return render(request, "inicio.html", contexto) # Cambien los nombres para los HTML... si es para asignaturas, debería ser ingresoasignatura.html
